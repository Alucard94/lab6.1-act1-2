package com.example.user.lab61_actionbar;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class Principal extends AppCompatActivity implements enviarMensaje{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Toast.makeText(Principal.this, "Settings", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_buscar:
                Toast.makeText(Principal.this, "Buscar", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_nuevo:
                Toast.makeText(Principal.this, "Nuevo", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_guardar:
                Toast.makeText(Principal.this, "Guardar", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_eliminar:
                Toast.makeText(Principal.this, "Eliminar", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_salir:
                Toast.makeText(Principal.this, "Salir", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void enviarDatos (String mensaje){
        Derecha derecha = (Derecha) getSupportFragmentManager().findFragmentById(R.id.derecha);
        derecha.obtenerDatos(mensaje);
    }
}
